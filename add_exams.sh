#!/usr/bin/env bash
# curl --data 'name=francais' http://localhost:6543/exams
# curl --data 'name=math' http://localhost:6543/exams
# curl --data 'name=Etudiant 1' http://localhost:6543/students
# curl --data 'name=Etudiant 2' http://localhost:6543/students
# curl --data 'name=Etudiant 3' http://localhost:6543/students
curl --data 'student_id=1&exam_id=2' http://localhost:6543/student-to-exam
curl --data 'student_id=1&exam_id=3' http://localhost:6543/student-to-exam
curl --data 'student_id=2&exam_id=2' http://localhost:6543/student-to-exam
