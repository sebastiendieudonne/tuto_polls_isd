import pyramid
from cornice.resource import resource, view
from polls.models.student import Student
from polls.models.exam import Exam


@resource(collection_path="/students", path="/student/{id}")
class StudentRessource:
    def __init__(self, request):
        self.request = request

    @view(renderer='json')
    def collection_get(self):
        """
        Method to get all students
        :return: exams collection
        """
        return Student.query().all()

    @view(renderer='json')
    def get(self):
        """
        method to get One student
        :return: an exam
        """
        try:
            id_ = int(self.request.matchdict['id'])
        except ValueError:
            raise pyramid.httpexceptions.HTTPNotAcceptable('Id not an integer')
        return Student.get_by_id(id_).exams

    @view(renderer="json", accept="text/json")
    def collection_post(self):
        """
        Method for adding an Student
        """
        student = Student()
        student.name = self.request.POST['name']
        student.add()
        return True


@resource(path="student-to-exam")
class StudentToExamRessource:
    def __init__(self, request):
        self.request = request

    @view(renderer="json", accept="text/json")
    def post(self):
        """
        Method for link between student and exam
        """
        student_id = self.request.POST['student_id']
        exam_id = self.request.POST['exam_id']

        exam = Exam.get_by_id(exam_id)
        student = Student.get_by_id(student_id)
        student.exams.append(exam)
        return True
