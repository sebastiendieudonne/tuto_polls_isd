""" Cornice services.
"""
from cornice import Service

from polls.models.question import Question

import polls.models


hello = Service(name='hello', path='/', description="Simplest app")
question = Service(name='question', path='/q', description="A simple question")
option = Service(name='option', path='/o', description="A simple option")


@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}


@question.post()
def add_question(request):
    q = Question()
    q.phrase = request.POST['question']
    polls.models.DBSession.add(q)
    return {'ok': True}


@question.get()
def get_question(request):
    if 'id' not in request.GET:
        return {'data': [
            q.phrase
            for q in Question.query().all()
            ]}
    qid = int(request.GET['id'])
    return Question.query().filter(Question.id == qid).one().phrase

