from . import Base

from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    ForeignKey,
)


class Question(Base):
    __tablename__ = "questions"
    id = Column(Integer, primary_key=True)
    phrase = Column(String(1024))
    coef = Column(Float)
    exam_id = Column(Integer, ForeignKey("exams.id"))

    def __init__(self, ph=None):
        self.phrase = ph

    def __json__(self):
        return {self.id, self.phrase, self.coef}


class Option(Base):
    __tablename__ = "options"
    id = Column(Integer, primary_key=True)
    name = Column(String(1024))
    question_id = Column(Integer, ForeignKey("questions.id"))
